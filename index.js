// IMPORT FETCH & DOTENV
const fetch = require('node-fetch');
require('dotenv').config();

// VARIABLES UTILES
const id = "194748482%40N07";
const set = "72177720295795434";
const api = process.env.FLICKR_API_KEY;
const flickr_url = `https://api.flickr.com/services/rest/?method=flickr.photosets.getPhotos&api_key=${api}&photoset_id=${set}&user_id=${id}&extras=url_k&format=json&nojsoncallback=1`;

// WEB SERVER & ROUTES
var express = require("express");
var path = require("path");

var routes = require("./routes");

var app = express();

app.set("port", process.env.PORT || 3000);
app.set("views", path.join(__dirname, "views"));
app.use(express.static(__dirname + '/dist'));
app.set("view engine", "ejs");

app.use(routes);

app.listen(app.get("port"), function(){
    console.log("Server started on port " + app.get("port"));
})

// BUILD DATABASE

const Datastore = require('nedb');

// DATABASE FILE - PERSISTENT
//const database = new Datastore({ filename: 'database.db', autoload: true });

//IN MEMORY - NON PERSISTENT
const database = new Datastore();
//database.loadDatabase();


// Flickr Get Photoset

app.get('/api', async (request, response) => {
  database.find({}, (err, data) => {
    response.json(data);
  });
}); 

function renderGalleryItem() {
  fetch(flickr_url)
    .then(response => response.json())
    .then((response) => {

      //let tactic = new Array(response.photoset.total - 1);
      //console.log(tactic)

      let imgArray = response.photoset.photo;

      //Create non persistent database
      database.insert(imgArray);
      //Count number of images.
      database.count({ server:"65535" }, function (err, count) {
        // count equals to 4 (5 - 1).
        //console.log(count - 1);
      });
      //Update imgArray if persistent values only.
      //database.update({},imgArray);
      //console.log(imgArray)

      for (var i = 0; i < imgArray.length; i++) {
        //console.log([response.photoset.photo[i].url_k]);
      }
    })
}
renderGalleryItem();
        


