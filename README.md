# 18 Editions.

Simple one page structure, based on ejs, dotenv, node-fetch, express, nedb, & boostrap 5 (vanilla JS) with img fetching from Flickr API (JS & Node JS).

Features : 

.Tested App node.js on heroku & glitch. 

.Fetching photos from other social API's.

.Design : some tests & changes remaining (ex : Multilang)…

***

## Screenshots

Page d'accueil unique / Homepage.

![Screenshot](screenshots/screenshot_accueil.jpg)

Boutique / Store.

![Screenshot](screenshots/screenshot_boutique.jpg)

Contact.

![Screenshot](screenshots/screenshot_contact.jpg)

CGV / General terms of sale.

![Screenshot](screenshots/screenshot_cgv.jpg)

***

## ©Photos

- Pexels & Unsplash…for concept presentation.
- The photos are stored in a public folder on flickr or another social media platform, showing the books the publisher produces or the photos contained in it.
- Ten photos maximum for a light configuration and for teasing.
- Boutique is on a marketplace.
- Et voilà ! 



