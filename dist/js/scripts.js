// CODE SERVER IS TODO //

//ASYNC AWAIT METHOD

async function getImgUrlsFromFlickr(req, res) {
    const resp = await fetch('/api');
    const data = await resp.json();

    document.getElementById("indicators").innerHTML += '<button type="button" data-bs-target="#diapo" data-bs-slide-to="0" class="active indic" aria-current="true" aria-label="Slide 1"></button>';
      for (var i = 1; i < data.length; i++) {
        document.getElementById("indicators").innerHTML += '<button type="button" data-bs-target="#diapo" data-bs-slide-to=' + [i] + ' class="indic" aria-current="true" aria-label="Slide' + [i + 1] + '"></button>';
        //console.log([i]);
    }
      document.getElementById("addslide").innerHTML += '<div class="carousel-item active h-100" style="background-image: url(' + data[0].url_k + ');"></div>';           
       for (var i = 1; i < data.length; i++) {
          document.getElementById("addslide").innerHTML += '<div class="carousel-item h-100" style="background-image: url(' + data[i].url_k + ');"></div>';
          //console.log(data[i].url_k);
      }
};
getImgUrlsFromFlickr();

 function toggle_full_screen() {
    if ((document.fullScreenElement && document.fullScreenElement !== null) || (!document.mozFullScreen && !document.webkitIsFullScreen)) {
        if (document.documentElement.requestFullScreen) {
            document.documentElement.requestFullScreen();
        }
        else if (document.documentElement.mozRequestFullScreen) { //Firefox
            document.documentElement.mozRequestFullScreen();
        }
        else if (document.documentElement.webkitRequestFullScreen) {   //Chrome, Safari & Opera 
            document.documentElement.webkitRequestFullScreen(Element.ALLOW_KEYBOARD_INPUT);
        }
        else if (document.msRequestFullscreen) { //IE/Edge
            document.documentElement.msRequestFullscreen();
        }
    }
    else {
        if (document.cancelFullScreen) {
            document.cancelFullScreen();
        }
        else if (document.mozCancelFullScreen) { //Firefox
            document.mozCancelFullScreen();
        }
        else if (document.webkitCancelFullScreen) {   //Chrome, Safari & Opera
            document.webkitCancelFullScreen();
        }
        else if (document.msExitFullscreen) { //IE/Edge
            document.msExitFullscreen();
        }
    }
}

document.addEventListener("DOMContentLoaded", validation);
// Example starter JavaScript for disabling form submissions if there are invalid fields
function validation() {
    'use strict'

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.querySelectorAll('.needs-validation')

    // Loop over them and prevent submission
    Array.prototype.slice.call(forms)
        .forEach(function (form) {
            form.addEventListener('submit', function (event) {
                if (!form.checkValidity()) {
                    event.preventDefault()
                    event.stopPropagation()
                }

                form.classList.add('was-validated')
            }, false)
        })
}
